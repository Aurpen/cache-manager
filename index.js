const fs = require("fs");
const { InternalServerError, NotFound, BadRequest } = require("http-response");
const { writeFile, readFile, unlink } = require("fs").promises;

class CacheManager {
  constructor(options) {
    this.setFolderPath(options);
    this.setRef(options);
    this.format = options?.format;
  }

  /**
   * Vérifie l'éxistence du dossier de cache
   * @param {Object} options
   */
  setFolderPath = (options) => {
    if (
      !options?.folderPath ||
      typeof options.folderPath !== "string" ||
      options.folderPath.trim() === ""
    ) {
      throw InternalServerError({
        error: { ...options },
        message: "CacheManager : folderPath value is invalid",
      });
    }

    if (!fs.existsSync(options.folderPath)) {
      throw InternalServerError({
        error: { ...options },
        message: "CacheManager : folderPath n'a pas de dossier correspondant",
      });
    }

    this.folderPath = options.folderPath;
  };

  /**
   * Vérifie la référence qui définie le périmètre du manager
   * @param {Object} options
   */
  setRef = (options) => {
    if (
      !options?.ref ||
      typeof options.ref !== "string" ||
      options.ref.trim() === ""
    ) {
      throw InternalServerError({
        error: { ...options },
        message: "CacheManager : Ref value is invalid",
      });
    } else {
      this.ref = options.ref;
    }
  };

  /**
   * Construit les infos du cache
   * @param {string} _id
   */
  fileInfo = (_id) => {
    const { folderPath } = this;
    const name = this.formatFileName(_id);
    const path = `${folderPath}/${name}`;
    return { _id, name, path };
  };

  /**
   * Transforme l'_id du cache en nom de fichier
   * @param {string} _id
   */
  formatFileName = (_id) => {
    const { ref, format } = this;
    return `${ref}.${_id}.${format}`;
  };

  /**
   * Récupère l'_id du cache
   * @param {string} file
   */
  extractId = (file) => {
    const { ref, format } = this;
    const extension = format ? `|.${format}` : "";
    const pattern = new RegExp(`${ref}.${extension}`, "g");
    return file.replace(pattern, "");
  };

  /**
   * Retourne un cache ou une 404
   * @param {String} _id
   * @return {Object} Request | Error
   */
  find = async (_id) => {
    const file = this.fileInfo(_id);
    try {
      return await readFile(file.path);
    } catch (error) {
      throw NotFound({ error: { ...file } });
    }
  };

  /**
   * Vérifie si le fichier est dans le périmètre du manager
   * @param {string} file
   */
  inPerimeter = (file) => {
    const { ref } = this;
    return new RegExp(ref).test(file);
  };

  /**
   * Récupère tous les caches dans le périmètre du manager
   * @return {Object} Request | Error
   */
  findAll = async () => {
    const { folderPath, extractId, inPerimeter } = this;
    try {
      const filenames = await fs.readdirSync(folderPath);
      const fileInfo = [];
      filenames.forEach((file) => {
        if (inPerimeter(file)) {
          const { mtime, size, birthtime } = fs.statSync(
            `${folderPath}/${file}`
          );
          const info = {
            name: extractId(file),
            created: birthtime,
            modified: mtime,
            size,
          };
          fileInfo.push(info);
        }
      });

      return fileInfo;
    } catch (error) {
      throw InternalServerError({ error: { ...error } });
    }
  };

  /**
   * Création d'un cache
   * @param {String} _id
   * @param {string} data
   */
  create = async (_id, data) => {
    if (typeof data !== "string") {
      throw BadRequest({ error: "data is not string type" });
    }

    const file = this.fileInfo(_id);

    // si le cache existe déjà on retourn un erreur
    if (fs.existsSync(file.path)) {
      throw BadRequest({
        error: { ...file },
        message: `${_id} cache allready exist`,
      });
    }

    try {
      return await writeFile(file.path, data);
    } catch (error) {
      throw InternalServerError({
        error: { ...file, ...error },
      });
    }
  };

  /**
   * Mise à jour d'un cache
   * @param {String} _id
   * @param {String} data
   */
  update = async (_id, data) => {
    const file = this.fileInfo(_id);

    // si le cache existe déjà on retourn un erreur
    if (!fs.existsSync(file.path)) {
      throw NotFound({
        error: { ...file },
        message: `${_id} cache was not found`,
      });
    }

    try {
      return await writeFile(file.path, data);
    } catch (error) {
      throw InternalServerError({ error: { ...file, ...error } });
    }
  };

  /**
   * Supprime un cache à partir de son _id
   * @param {string} _id
   */
  delete = async (_id) => {
    const file = this.fileInfo(_id);
    try {
      return await unlink(file.path);
    } catch (error) {
      throw NotFound({ error: { ...file } });
    }
  };
}

module.exports = (options) => {
  return new CacheManager(options);
};
